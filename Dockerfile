ARG PYTHON_VERSION=3.6-slim
FROM python:$PYTHON_VERSION as base

WORKDIR /agent

RUN apt-get update && apt-get install -y jq

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

# For python version >= 3.10, replace collections module with collections.abc in /tornado/httputil.py
RUN if [ $(python -c 'import sys; print(sys.version_info[:2] >= (3, 10))') = "True" ]; then \
    sed -i 's/collections.MutableMapping/collections.abc.MutableMapping/g' /usr/local/lib/python3.*/site-packages/tornado/httputil.py; \
    fi

COPY . .

CMD [ "bash", "bin/set_config_and_run.sh" ]


FROM base as test

RUN cd test && ./runtests.sh
