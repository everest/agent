#!/bin/bash
set -e
if [ "$EVEREST_TOKEN" == "" ]; then
    echo "EVEREST_TOKEN environment variable not set. Exiting."
    exit 1
fi
if [ "$MAX_TASKS" == "" ]; then
    MAX_TASKS=1
fi
mkdir -p conf
jq \
    ".protocol.activeMode.agentToken = \"$EVEREST_TOKEN\" | \
    .resource.maxTasks = $MAX_TASKS | \
    .taskProtocol.enabled = true | \
    .taskProtocol.address = \"127.0.0.1\"" \
    everest_agent/agent.conf.default > conf/agent.conf
#cat conf/agent.conf
exec python -m everest_agent.start -l /dev/stdout
