# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from __future__ import absolute_import
import json

from .exceptions import ConfigException
import six

PATHS_SRC = {
    "resource": {
        "tasksDir": "tasksDir",
        "taskHandler": "taskHandler",
        "maxTasks": "maxTasks",
        "checkTime": 'checkTime',
        "utilityPoolSize": "utilityPoolSize",
        "keepTaskDir": "keepTaskDir",
        "taskDirCleanupPeriod": "taskDirCleanupPeriod",
        "maxTaskDirSize": "taskDirSize",
        "attributes": "attributes"
    },
    "protocol": {
        "activeMode": {
            "enabled": "activeModeEnabled",
            "serverURI": "serverURI",
            "agentToken": "agentToken"
        },
        "passiveMode": {
            "enabled": "passiveModeEnabled",
            "agentPort": 'agentPort',
            "clientTokens": 'clientTokens',
            "uploadDir": "uploadDir"
        },
        "http" : {
            "maxUploadSize" : "maxBodySize",
            'requestTimeout' : 'requestTimeout',
            'connectTimeout' : 'connectTimeout',
            'requestTries' : 'requestTries',
            "cacheDir" : "cacheDir",
            "maxCacheSize" : "maxCacheSize"
        },
        "resourceInfoPeriod": 'resourceInfoPeriod',
        "pingPeriod": 'pingPeriod',
        "pongTimeout": 'pongTimeout'
    },
    "security": {
        "allowedCommands": 'whitelist'
    },
    "logging": {
        "logDir": "logDir",
        "level": "logLevel",
        "rotate": "logRotate",
        "maxSize": "logMaxBytes",
        "backupCount": "logBackupCount"
    },
    "controlAPI": {
        "enabled": 'controlAPIEnabled',
        "port": 'controlPort'
    },
    "taskProtocol": {
        "enabled": 'taskProtocolEnabled',
        "address": 'taskAddress',
        "port": 'taskPort'
    },
    "proxy": {
        "enabled": 'proxyEnabled',
        "frontendURI" : "proxyFrontendURI",
        "backendToken" : "proxyBackendToken"
    }
}

DEFAULTS = {
    'whitelist' : None,
    'proxyEnabled' : False,
    'proxyFrontendURI' : None,
    'proxyBackendToken' : None,
    'taskAddress' : 'localhost',
    'taskDirCleanupPeriod' : 300,
    'taskDirSize' : 100000000,
    'requestTimeout' : 60,
    'connectTimeout' : 30,
    'requestTries' : 5
}

def buildPaths(d, path):
    if not type(d) is dict:
        return {d : path}
    result = {}
    for k, v in six.iteritems(d):
        result.update(buildPaths(v, path + [k]))
    return result

class Config(object):
    def __init__(self):
        self.paths = buildPaths(PATHS_SRC, [])
        self.params = {}

    def update(self, fname):
        self.paths = buildPaths(PATHS_SRC, [])
        with open(fname, 'r') as f:
            try:
                d = json.load(f)
            except ValueError as e:
                raise ConfigException('failed to parse %s: %s' % (fname, e))

        for key, path in six.iteritems(self.paths):
            val = d
            try:
                for k in self.paths[key]:
                    val = val[k]
                self.params[key] = val
            except KeyError:
                pass

    def check(self):
        for key, path in six.iteritems(self.paths):
            self.get(key)
        if self.get('activeModeEnabled') and not self.get('agentToken').isalnum():
            raise ConfigException('agentToken parameter must be alphanumeric')
        if self.get('passiveModeEnabled'):
            for token in self.get('clientTokens'):
                if not token.isalnum():
                    raise ConfigException('clientToken "%s" is not alphanumeric' % token)

    def useActiveMode(self):
        return self.get('activeModeEnabled')

    def usePassiveMode(self):
        return self.get('passiveModeEnabled')

    def useControlAPI(self):
        return self.get('controlAPIEnabled')

    def useTaskProtocol(self):
        return self.get('taskProtocolEnabled')

    def get(self, key):
        assert(key in self.paths)
        if not key in self.params:
            if key in DEFAULTS:
                return DEFAULTS[key]
            raise ConfigException('parameter %s not found in config' % '.'.join(self.paths[key]))
        return self.params[key]

    def getTaskHandlerConf(self):
        result = dict(self.get('taskHandler'))
        result['maxTasks'] = self.get('maxTasks')
        result['checkTime'] = self.get('checkTime')
        return result

    def dump(self):
        result = {}
        for key, val in six.iteritems(self.params):
            obj = result
            for k in self.paths[key][:-1]:
                if not k in obj:
                    obj[k] = {}
                obj = obj[k]
            obj[self.paths[key][-1]] = val
        return result

    def setParam(self, key, value):
        assert(key in self.paths)
        self.params[key] = value

    def delParam(self, key):
        assert(key in self.paths)
        del self.params[key]
