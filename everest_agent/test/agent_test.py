# coding: utf-8
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from __future__ import absolute_import
import os
import subprocess
import sys
import traceback
import json
import shutil
import tarfile
import zipfile
import time
import unittest
import hashlib
from contextlib import closing 

import tornado
from tornado import gen
from tornado import ioloop
from tornado.httpclient import HTTPError
from tornado.websocket import WebSocketHandler, websocket_connect
from tornado import web
from tornado import testing
from tornado import httpclient

import psutil

from everest_agent.constants import *
from everest_agent.test.constants import *
from everest_agent.config import Config
from everest_agent.utils import fixUTF8InURI
import six
from six.moves import range

PORT = 8893

def sendMessage(ws, msgType, *args):
    ws.write_message(json.dumps([msgType] + list(args)))

def fetchRelativeURI(uri, **kwargs):
    with closing(httpclient.HTTPClient()) as client:
        response = client.fetch('http://localhost:%d%s' % (PORT, uri), headers = {
            'Authorization' : 'EverestAgentClientToken 1:67890'}, **kwargs)
        return response

class MyHandler(object):
    def __init__(self, app):
        self.app = app

    def on_connect(self, future):
        conn = future.result()
        self.app.ws = conn
        assert(conn.headers['Sec-WebSocket-Protocol'] == 'v1.agent.everest')
        sendMessage(conn, 'HELLO', {})

    def on_message(self, msg):
        if msg is None:
            self.app.ws_state = 2
            self.app.test.stop()
            return
        msg = json.loads(msg)
        if self.app.ws_state == 0 or self.app.ws_state == 3:
            assert(len(msg) == 3)
            assert(msg[0] == 'WELCOME')
            assert('version' in msg[1])
            assert('system' in msg[1])
            assert(msg[2]['type'] == 'local')
            assert(msg[2]['slots']['total'] == 1)
            assert(msg[2]['slots']['free'] == 1)
            if self.app.ws_state == 0:
                assert(msg[2]['tasks']['total'] == 0)
            assert(msg[2]['tasks']['running'] == 0)
            assert(msg[2]['tasks']['maxRunning'] == 1)
            self.app.ws_state = 1
            self.app.test.stop()
        elif self.app.ws_state == 1:
            if msg[0] == 'TASK_STATE':
                assert(len(msg) == 4)
                if msg[2] == 'ACCEPTED':
                    assert(msg[3] == {})
                elif msg[2] == 'STAGED_IN':
                    assert('stageInBytes' in msg[3])
                    assert('stageInTime' in msg[3])
                elif msg[2] == 'QUEUED':
                    assert(msg[3] == {})
                elif msg[2] == 'RUNNING':
                    assert('waitTime' in msg[3])
                elif msg[2] == 'COMPLETED':
                    assert('runTime' in msg[3])
                elif msg[2] == 'STAGED_OUT':
                    assert('stageOutBytes' in msg[3])
                    assert('stageOutTime' in msg[3])
                    assert('outputData' in msg[3])
                elif msg[2] == 'DONE':
                    assert('processTime' in msg[3])
                elif msg[2] == 'FAILED':
                    assert('processTime' in msg[3])
                    assert('reason' in msg[3])
                elif msg[2] == 'CANCELED':
                    assert('processTime' in msg[3])
                    assert('reason' in msg[3])
                elif msg[2] == 'DELETED':
                    assert(msg[3] == {})
                else:
                    assert(False)
                self.app.ws_tasks.append([msg[1], msg[2], msg[3], False])
            elif msg[0] == 'RESOURCE_INFO':
                return
            elif msg[0] == 'TASK_MESSAGE':
                self.app.ws_task_msgs.append((msg[1], msg[2], msg[3]))
            elif msg[0] in ['STAGEIN_RESP', 'STAGEOUT_RESP', 'LISTDIR_RESP']:
                self.app.ws_other_msgs.append(msg)
            else:
                assert(False)
            self.app.test.stop()
        elif self.app.ws_state == 2:
            assert(False) # Connection has already closed

class WSClientHandler(WebSocketHandler):
    def select_subprotocol(self, subprotocols):
        auth = self.request.headers['Authorization']
        assert(auth == 'EverestAgentToken 12345')

        if not PROTOCOL_VERSION in subprotocols:
            return None
        return PROTOCOL_VERSION

    def open(self):
        self.myHandler = MyHandler(self.application)
        self.application.ws = self
        sendMessage(self, 'HELLO', {})

    def on_message(self, msg):
        self.myHandler.on_message(msg)

    def on_close(self):
        self.myHandler.on_message(None)

class FileHandler(web.RequestHandler):
    SUPPORTED_METHODS = ('PUT', 'GET', 'DELETE')

    def initialize(self, file_root, auth):
        self.file_root = file_root
        self.auth = auth

    def get(self, fname):
        if self.auth and self.request.headers.get('Authorization', '') != self.auth:
            raise web.HTTPError(403)
        path = os.path.join(self.file_root, fname)
        if not os.path.isfile(path):
            raise web.HTTPError(404)
        with open(path, 'rb') as f:
            self.finish(f.read())

    def delete(self, fname):
        if self.auth and self.request.headers.get('Authorization', '') != self.auth:
            raise web.HTTPError(403)
        path = os.path.join(self.file_root, fname)
        if not os.path.isfile(path):
            raise web.HTTPError(404)
        os.remove(path)
        self.set_status(204)
        self.finish()

    def put(self, fname):
        if self.auth and self.request.headers.get('Authorization', '') != self.auth:
            raise web.HTTPError(403)
        path = os.path.normpath(os.path.join(self.file_root, fname))
        if not os.path.dirname(path).startswith(self.file_root):
            raise web.HTTPError(403)
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))
        with open(path, 'wb') as f:
            f.write(self.request.body)
        self.finish()

class SlowHandler(web.RequestHandler):
    def initialize(self, file_root):
        self.file_root = file_root

    @gen.coroutine
    def get(self, fname):
        path = os.path.join(self.file_root, fname)
        if not os.path.isfile(path):
            raise web.HTTPError(404)
        yield gen.sleep(1)
        with open(path, 'rb') as f:
            self.finish(f.read())

    @gen.coroutine
    def put(self, fname):
        path = os.path.normpath(os.path.join(self.file_root, fname))
        if not os.path.dirname(path).startswith(self.file_root):
            raise web.HTTPError(403)
        yield gen.sleep(1)
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))
        with open(path, 'wb') as f:
            f.write(self.request.body)
        self.finish()


class AgentTestServer(testing.AsyncHTTPTestCase):
    def get_app(self):
        self.file_root = 'test_root'
        app = web.Application([
            (r"/compute/files/(.+)", FileHandler,
             {'file_root' : self.file_root, 'auth' : ''}),
            (r"/auth/(.+)", FileHandler,
             {'file_root' : self.file_root, 'auth' : 'CoolMethod 12345'}),
            (r"/slow/(.+)", SlowHandler, {'file_root' : self.file_root}),
            (r"/compute/ws", WSClientHandler),
        ])
        app.ws_state = 0
        app.ws_tasks = []
        app.ws_task_msgs = []
        app.ws_other_msgs = []
        app.test = self
        self.app = app
        if not os.path.exists(self.file_root):
            os.makedirs(self.file_root)
        return app

    def uploadFile(self, fname):
        with open(fname, 'rb') as f:
            fileData = f.read()
        with closing(httpclient.HTTPClient()) as client:
            response = client.fetch(
                'http://localhost:%d/agent/upload' % PORT,
                method = 'POST', body = fileData, headers = {
                    'Authorization' : 'EverestAgentClientToken 1:67890'})
            self.assertEqual(201, response.code)
            return response.headers['Location']

    def setUp(self, moreParams={}):
        testing.AsyncHTTPTestCase.setUp(self)
        config = Config()
        config.update('../everest_agent/agent.conf.default')
        config.setParam('agentToken', '12345')
        config.setParam('whitelist', ['hostname', '{} .*dummy.py.*'.format(sys.executable),
                                      'command_that_does_not_exist'])
        config.setParam('checkTime', 0.1)
        config.setParam('clientTokens', ['67890'])
        config.setParam('pingPeriod', 0.2)
        config.setParam('pongTimeout', 5)
        config.setParam('serverURI', 'ws://localhost:%d/compute/ws' % self.get_http_port())
        config.setParam('activeModeEnabled', self.agentMode == 'active')
        config.setParam('passiveModeEnabled', True)
        config.setParam('taskProtocolEnabled', True)
        config.setParam('taskAddress', '127.0.0.1')
        config.setParam('agentPort', PORT)
        config.setParam('proxyEnabled', True)
        #config.setParam('keepTaskDir', True)
        #config.setParam('proxyBackendToken', '1234')

        for k, v in six.iteritems(moreParams):
            config.setParam(k, v)

        if not os.path.exists('conf'):
            os.makedirs('conf')
        with open(os.path.join('conf', 'agent.conf'), 'w') as f:
            json.dump(config.dump(), f)
        self.taskId = 0
        self.assertEqual(0, self.app.ws_state)
        self.agent_proc = subprocess.Popen([sys.executable, '-m', 'everest_agent.start',
                                            '-c', 'conf/agent.conf'])
        try:
            if self.agentMode == 'passive':
                time.sleep(1)
                self.ws_req = tornado.httpclient.HTTPRequest(
                    'ws://localhost:%d/agent/ws' % PORT, validate_cert=False,
                    headers = {'Authorization' : 'EverestAgentClientToken 1:67890',
                               'Sec-WebSocket-Protocol' : 'v1.agent.everest'})
                self.ws_handler = MyHandler(self.app)
                # May fail if agent is not listening yet. TODO: add a retry in error handler
                future = websocket_connect(self.ws_req, on_message_callback = self.ws_handler.on_message)
                ioloop.IOLoop.current().add_future(future, self.ws_handler.on_connect)
            
            self.wait() # wait for agent connection
            self.assertEqual(1, self.app.ws_state)
        except:
            self.agent_proc.terminate()
            raise

    def tearDown(self):
        try:
            for i in range(1, self.taskId + 1):
                sendMessage(self.app.ws, 'TASK_DELETE', str(i))
                self.checkTaskStates(str(i), ['DELETED'])
                pass
        finally:
            self.agent_proc.terminate()
        self.agent_proc.wait()
        testing.AsyncHTTPTestCase.tearDown(self)
        shutil.rmtree(self.file_root)
        if os.path.isdir('cache'):
            shutil.rmtree('cache')

    def doSubmit(self, command, outputFiles = ['stdout'], inputs = [],
                 uploadOutputs = True, packOutputs = True, inputData = [], outputData = []):
        self.taskId = self.taskId + 1
        taskContext = {
            'user' : 'TODO',
            'application' : 'TODO'
        }
        taskSpec = {
            "command" : command,
            "inputData" : inputData,
            "outputData" : [{
                "paths": outputFiles
            }]}

        if packOutputs:
            taskSpec['outputData'][0]['pack'] = "output.tar.gz"
        if uploadOutputs:
            taskSpec['outputData'][0]['uri'] = self.get_url('/compute/files/%d/output.tar.gz' % self.taskId)
        if outputData:
            taskSpec['outputData'] = outputData
        if inputs:
            taskSpec['inputData'] = [{
                'uri' : fixUTF8InURI(self.get_url("/compute/files/%s" % inp)),
                'path' : inp,
                'unpack' : '.'} for inp in inputs]
        sendMessage(self.app.ws, 'TASK_SUBMIT', str(self.taskId), taskSpec, taskContext)

    def doPythonSubmit(self, command, *args, **kwargs):
        return self.doSubmit(six.u('{} {}').format(sys.executable, command), *args, **kwargs)

    def checkTaskStates(self, taskId, states):
        for s in states:
            self.wait()
            self.assertEqual((str(taskId), s), (self.app.ws_tasks[-1][0],
                                                self.app.ws_tasks[-1][1]))
            self.app.ws_tasks[-1][3] = True

    def checkTaskStatesSoft(self, taskId, states):
        statesCopy = list(states)
        while statesCopy:
            inbox = [s for s in self.app.ws_tasks
                     if not s[3] and s[0] == str(taskId)]
            if not inbox:
                self.wait()
                continue
            self.assertEqual(statesCopy[0], inbox[0][1], inbox)
            inbox[0][3] = True
            statesCopy = statesCopy[1:]

    def makeInput(self, name):
        inPath = os.path.join(self.file_root, name + '.tar.gz')
        with closing(tarfile.open(inPath, 'w:gz')) as ar:
            ar.add('runtests.sh', name + '.txt')
        return name + '.tar.gz'

class AgentTests(AgentTestServer):
    def testHostname(self):
        self.doSubmit('hostname')
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING', 'COMPLETED', 'STAGED_OUT', 'DONE'])

    def testSameId(self):
        "When server submits a task with the same ID twice (e.g. after a restart)"
        self.doPythonSubmit('%s 1 1 2 0 0' % DUMMY_PATH)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING'])
        self.taskId -= 1
        self.doPythonSubmit('%s 1 1 1 0 0' % DUMMY_PATH)
        self.checkTaskStates(self.taskId, ['FAILED', 'COMPLETED', 'STAGED_OUT', 'DONE'])

    def testOutputFiles(self):
        "Testing basic scenario: single process, correct completion, writing some info to STDOUT and STDERR"
        outVal = 4822759
        errVal = 3850603
        self.doPythonSubmit('%s %d %d 0.1 0 0' % (DUMMY_PATH, outVal, errVal),
                      ['stdout', 'stderr'])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING', 'COMPLETED', 'STAGED_OUT', 'DONE'])
        outPath = os.path.join(self.file_root, str(self.taskId), 'output.tar.gz')
        self.assertTrue(os.path.isfile(outPath))
        with closing(tarfile.open(outPath, 'r:gz')) as ar:
            o = ar.extractfile('stdout')
            self.assertEqual(outVal, int(o.readline()))
            e = ar.extractfile('stderr')
            self.assertEqual(errVal, int(e.readline()))

    def testSingleInputFile(self):
        inp = self.makeInput('input')
        inputData = [{'uri' : self.get_url("/compute/files/%s" % inp),
                      'path' : inp,
                      'unpack' : 'data'}]
        self.doPythonSubmit('%s -1 0 0.1 0 0 data/input.txt' % DUMMY_PATH, ['stdout'], inputData = inputData)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'STAGED_IN', 'RUNNING', 'COMPLETED', 'STAGED_OUT', 'DONE'])
        outPath = os.path.join(self.file_root, str(self.taskId), 'output.tar.gz')
        with closing(tarfile.open(outPath, 'r:gz')) as ar:
            with open('runtests.sh', 'rb') as expf:
                resf = ar.extractfile('stdout')
                # Workaround for dummy.py printing to stdout in text mode
                self.assertEqual([l.strip() for l in expf.readlines()],
                                 [l.strip() for l in resf.readlines()])

    def testSingleInputFileDelete(self):
        inp = self.makeInput('input')
        inputData = [{'uri' : self.get_url("/compute/files/%s" % inp),
                      'path' : inp,
                      'unpack' : 'data',
                      'delete' : True}]
        self.doPythonSubmit('%s -1 0 0.1 0 0 data/input.txt' % DUMMY_PATH, ['stdout'], inputData = inputData)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'STAGED_IN', 'RUNNING', 'COMPLETED', 'STAGED_OUT', 'DONE'])
        sendMessage(self.app.ws, 'TASK_DELETE', str(self.taskId))
        self.checkTaskStates(self.taskId, ['DELETED'])
        self.assertFalse(os.path.isfile(os.path.join(self.file_root, inp)))

    def testDeleteRunning(self):
        self.doPythonSubmit('%s 0 0 120 0 0' % DUMMY_PATH)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING'])
        sendMessage(self.app.ws, 'TASK_DELETE', str(self.taskId))
        self.checkTaskStates(self.taskId, ['CANCELED', 'DELETED'])

    def testTwoInputFiles(self):
        inputs = [self.makeInput('input0'), self.makeInput('input1')]
        self.doPythonSubmit('%s -1 0 0.1 0 0 input0.txt input1.txt' % DUMMY_PATH,
                      ['stdout'], inputs = inputs)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'STAGED_IN', 'RUNNING', 'COMPLETED', 'STAGED_OUT', 'DONE'])
        outPath = os.path.join(self.file_root, str(self.taskId), 'output.tar.gz')
        with closing(tarfile.open(outPath, 'r:gz')) as ar:
            with open('runtests.sh', 'rb') as f:
                o = ar.extractfile('stdout')
                # Workaround for dummy.py printing to stdout in text mode
                exp = [l.strip() for l in f.readlines()]
                self.assertEqual(exp + exp, [l.strip() for l in o.readlines()])

    def testBadInput(self):
        self.doPythonSubmit('%s -1 0 1 0 0' % DUMMY_PATH, ['stdout'], inputs = ['input123.tar.gz'])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'FAILED'])
        self.assertEqual(6, self.app.ws_tasks[-1][2]['errorCode'])

    def testBadInputThreeFiles(self):
        inputs = [self.makeInput('input0'), 'input123.tar.gz', self.makeInput('input1')]
        self.doPythonSubmit('%s -1 0 1 0 0' % DUMMY_PATH, ['stdout'], inputs = inputs)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'FAILED'])
        self.assertEqual(6, self.app.ws_tasks[-1][2]['errorCode'])

    # (failing tasks with missing output files is currently disabled as too restrictive)
    # def testBadOutput(self):
    #     self.doSubmit('hostname', ['noop.txt'])
    #     self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING', 'COMPLETED', 'STAGED_OUT', 'FAILED'])
    #     outPath = os.path.join(self.file_root, str(self.taskId), 'output.tar.gz')
    #     try:
    #         with closing(tarfile.open(outPath, 'r:gz')) as ar:
    #             self.assertEqual(0, len(ar.getnames()))
    #     except tarfile.ReadError, e:
    #         if not 'empty header' in str(e):
    #             raise

    def testBadCommand(self):
        self.doSubmit('command_that_does_not_exist', ['stdout'])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'FAILED'])
        self.assertEqual(6, self.app.ws_tasks[-1][2]['errorCode'])

    def testNotInWhitelist(self):
        self.doSubmit('command_not_in_whitelist', ['stdout'])
        self.checkTaskStates(self.taskId, ['FAILED'])
        self.assertEqual(2, self.app.ws_tasks[-1][2]['errorCode'])

    def testFailedTask(self):
        "Testing failed task scenario: single process, abnormal return code"
        self.doPythonSubmit('%s 0 0 0.1 0 1' % DUMMY_PATH)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING', 'COMPLETED', 'STAGED_OUT', 'FAILED'])
        failData = self.app.ws_tasks[-1][2]
        self.assertEqual(1, failData['exitCode'])
        self.assertEqual(1, failData['errorCode'])

    def testOrphan(self):
        "Testing orphan scenario: two processes, parent process finishes before child"
        pidsBefore = psutil.pids()
        self.doPythonSubmit('%s 0 0 2 10 0' % DUMMY_PATH)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING'])
        time.sleep(1) # wait for child process to start
        taskPids = []
        for p in psutil.process_iter():
            if not p.pid in pidsBefore and 'python' in p.name().lower():
                if 'dummy.py' in ' '.join(p.cmdline()):
                    taskPids.append(p.pid)
        self.assertEqual(2, len(taskPids))
        self.checkTaskStates(self.taskId, ['COMPLETED', 'STAGED_OUT', 'DONE'])
        self.assertFalse(psutil.pid_exists(taskPids[0]))
        self.assertFalse(psutil.pid_exists(taskPids[1]))

    def testCancel(self):
        "Testing cancel scenario: two processes that run too long"
        pidsBefore = psutil.pids()
        self.doPythonSubmit('%s 0 0 120 120 0' % DUMMY_PATH)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING'])
        time.sleep(1) # wait for child process to start
        taskPids = []
        for p in psutil.process_iter():
            if p.pid not in pidsBefore and 'python' in p.name().lower():
                if 'dummy.py' in ' '.join(p.cmdline()):
                    taskPids.append(p.pid)
        self.assertEqual(2, len(taskPids))
        sendMessage(self.app.ws, 'TASK_CANCEL', str(self.taskId))
        self.checkTaskStates(self.taskId, ['CANCELED'])
        self.assertFalse(psutil.pid_exists(taskPids[0]))
        self.assertFalse(psutil.pid_exists(taskPids[1]))

    def testStageInWhenOtherRunning(self):
        "Queued tasks should download input files while there are no free slots"
        inputs = [self.makeInput('input')]
        self.doPythonSubmit('%s 0 0 1 0 0' % DUMMY_PATH)
        id1 = self.taskId
        self.checkTaskStates(id1, ['ACCEPTED', 'RUNNING'])
        self.doPythonSubmit('%s 0 0 0.1 0 0' % DUMMY_PATH, inputs = inputs)
        id2 = self.taskId
        self.checkTaskStates(id2, ['ACCEPTED', 'STAGED_IN'])
        self.checkTaskStates(id1, ['COMPLETED'])
        self.checkTaskStates(id2, ['RUNNING'])
        self.checkTaskStatesSoft(id1, ['STAGED_OUT', 'DONE'])
        self.checkTaskStatesSoft(id2, ['COMPLETED', 'STAGED_OUT', 'DONE'])

    def testComplex(self):
        self.testNotInWhitelist()
        self.testHostname()
        self.testOutputFiles()
        self.testSingleInputFile()
        self.testTwoInputFiles()
        self.testBadInput()
        self.testBadInputThreeFiles()
        # self.testBadOutput()
        self.testBadCommand()
        self.testNotInWhitelist()
        self.testFailedTask()
        self.testOrphan()
        self.testCancel()
        self.testHostname()

    def testBadFilePathInInput(self):
        inPath = os.path.join(self.file_root, 'input.tar.gz')
        with closing(tarfile.open(inPath, 'w:gz')) as ar:
            ar.add('runtests.sh', '../input.txt')
        self.doPythonSubmit('%s -1 0 0.1 0 0 ../input.txt' % DUMMY_PATH,
                      ['stdout'], inputs = ['input.tar.gz'])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'FAILED'])
        self.assertEqual(6, self.app.ws_tasks[-1][2]['errorCode'])

    def testZipInput(self):
        inPath = os.path.join(self.file_root, 'input.zip')
        with closing(zipfile.ZipFile(inPath, 'w')) as ar:
            ar.write('runtests.sh', 'input.txt')
        self.doPythonSubmit('%s -1 0 0.1 0 0 input.txt' % DUMMY_PATH,
                      ['stdout'], inputs = ['input.zip'])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'STAGED_IN', 'RUNNING',
                                           'COMPLETED', 'STAGED_OUT', 'DONE'])

    def testBadZipInput(self):
        inPath = os.path.join(self.file_root, 'input.zip')
        with closing(zipfile.ZipFile(inPath, 'w')) as ar:
            ar.write('runtests.sh', '../input.txt')
        self.doPythonSubmit('%s -1 0 0.1 0 0 ../input.txt' % DUMMY_PATH,
                      ['stdout'], inputs = ['input.zip'])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'FAILED'])
        self.assertEqual(6, self.app.ws_tasks[-1][2]['errorCode'])

    def testSelfHostedPackedOutput(self):
        outVal = 4822759
        errVal = 3850603
        self.doPythonSubmit('%s %d %d 0.1 0 0' % (DUMMY_PATH, outVal, errVal),
                      ['stdout', 'stderr'], uploadOutputs = False)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING', 'COMPLETED', 'STAGED_OUT', 'DONE'])
        out = self.app.ws_tasks[-2][2]['outputData']
        self.assertEqual(1, len(out))
        outPath = os.path.join(self.file_root, out[0]['path'])
        with open(outPath, 'wb') as f:
            f.write(fetchRelativeURI(out[0]['uri']).body)
        with closing(tarfile.open(outPath, 'r:gz')) as ar:
            o = ar.extractfile('stdout')
            self.assertEqual(outVal, int(o.readline()))
            e = ar.extractfile('stderr')
            self.assertEqual(errVal, int(e.readline()))

    def testSelfHostedOutputNoPacking(self):
        outVal = 4822759
        errVal = 3850603
        vals = {'stdout' : outVal, 'stderr' : errVal}
        self.doPythonSubmit('%s %d %d 0.1 0 0' % (DUMMY_PATH, outVal, errVal),
                      ['stdout', 'stderr'], uploadOutputs = False, packOutputs = False)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING', 'COMPLETED', 'STAGED_OUT', 'DONE'])
        out = self.app.ws_tasks[-2][2]['outputData']
        self.assertEqual(2, len(out))
        for o in out:
            self.assertEqual(vals[o['path']], int(fetchRelativeURI(o['uri']).body.strip()))

    def testMixedOutput(self):
        outVal = 4822759
        errVal = 3850603
        vals = {'stdout' : outVal, 'stderr' : errVal}
        self.doPythonSubmit('%s %d %d 0.1 0 0' % (DUMMY_PATH, outVal, errVal),
                      outputData = [{'paths' : ['stdout', 'stderr']},
                                    {'paths' : ['stdout', 'stderr'],
                                     'pack' : 'output.tar.gz',
                                     'uri' : self.get_url('/compute/files/%d/output.tar.gz' % (self.taskId + 1))}])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING', 'COMPLETED', 'STAGED_OUT', 'DONE'])
        out = self.app.ws_tasks[-2][2]['outputData']
        self.assertEqual(3, len(out))
        for o in out:
            if o['uri'][0] == '/':
                self.assertEqual(vals[o['path']], int(fetchRelativeURI(o['uri']).body.strip()))
            else:
                outPath = os.path.join(self.file_root, str(self.taskId), 'output.tar.gz')
                self.assertTrue(os.path.isfile(outPath))
                with closing(tarfile.open(outPath, 'r:gz')) as ar:
                    o = ar.extractfile('stdout')
                    self.assertEqual(outVal, int(o.readline()))
                    e = ar.extractfile('stderr')
                    self.assertEqual(errVal, int(e.readline()))

    def testUpload(self):
        loc = self.uploadFile('runtests.sh')
        self.doPythonSubmit('%s -1 0 0.1 0 0 input.txt' % DUMMY_PATH, ['stdout'],
                      inputData = [{'uri' : loc, 'path' : 'input.txt'}],
                      uploadOutputs = False, packOutputs = False)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'STAGED_IN', 'RUNNING', 'COMPLETED', 'STAGED_OUT', 'DONE'])
        out = self.app.ws_tasks[-2][2]['outputData']
        self.assertEqual(1, len(out))
        with open('runtests.sh', 'rb') as f:
            fileData = f.read()
        self.assertEqual(fileData, fetchRelativeURI(out[0]['uri']).body.decode().replace('\r', '').encode())
        self.assertEqual(fetchRelativeURI(loc, method='DELETE').code, 204)

    def testAutoDeleteUploaded(self):
        loc = self.uploadFile('runtests.sh')
        self.doPythonSubmit('%s -1 0 0.1 0 0 input.txt' % DUMMY_PATH, ['stdout'],
                      inputData = [{'uri' : loc, 'path' : 'input.txt', 'delete' : True}],
                      uploadOutputs = False, packOutputs = False)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'STAGED_IN', 'RUNNING', 'COMPLETED', 'STAGED_OUT', 'DONE'])
        self.assertEqual(fetchRelativeURI(loc).code, 200)
        sendMessage(self.app.ws, 'TASK_DELETE', str(self.taskId))
        self.checkTaskStates(self.taskId, ['DELETED'])
        try:
            fetchRelativeURI(loc)
            self.fail('No HTTPError thrown')
        except httpclient.HTTPError:
            pass

    def testUploadedFileCheck(self):
        with open('runtests.sh', 'rb') as f:
            sha1 = hashlib.sha1(f.read()).hexdigest()
        try:
            fetchRelativeURI('/agent/filecheck?sha1=%s' % sha1, method='HEAD')
            self.fail('No HTTPError thrown')
        except httpclient.HTTPError:
            pass

        loc = self.uploadFile('runtests.sh')

        try:
            fetchRelativeURI('/agent/filecheck?sha1=%s' % sha1,
                                  method='HEAD', follow_redirects=False)
            self.fail('Must throw error due to redirect')
        except httpclient.HTTPError as e:
            self.assertEqual(e.response.code, 301)
            self.assertEqual(e.response.headers['Location'], loc)

    def testTaskMessage(self):
        self.doPythonSubmit('%s hello world' % TM_TEST_PATH, ['stdout'])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING'])
        self.wait()
        self.assertEqual(self.app.ws_task_msgs[0], (str(self.taskId), 0, 'hello'))
        sendMessage(self.app.ws, 'TASK_MESSAGE', str(self.taskId), 0, 'world')
        self.checkTaskStates(self.taskId, ['COMPLETED', 'STAGED_OUT', 'DONE'])
        outPath = os.path.join(self.file_root, str(self.taskId), 'output.tar.gz')
        self.assertTrue(os.path.isfile(outPath))
        with closing(tarfile.open(outPath, 'r:gz')) as ar:
            o = ar.extractfile('stdout').read().decode()
            self.assertTrue("Received world" in o, o)

    def testEarlyTaskMessage(self):
        self.doPythonSubmit('%s hello world' % TM_TEST_PATH, ['stdout'])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING'])
        sendMessage(self.app.ws, 'TASK_MESSAGE', str(self.taskId), 0, 'world')
        self.wait()
        self.assertEqual(self.app.ws_task_msgs[0], (str(self.taskId), 0, 'hello'))
        self.checkTaskStates(self.taskId, ['COMPLETED', 'STAGED_OUT', 'DONE'])
        outPath = os.path.join(self.file_root, str(self.taskId), 'output.tar.gz')
        self.assertTrue(os.path.isfile(outPath))
        with closing(tarfile.open(outPath, 'r:gz')) as ar:
            o = ar.extractfile('stdout').read().decode()
            self.assertTrue("Received world" in o, o)

    def testReconnect(self):
        # don't upload outputs to avoid race condition in test: can't handle file 
        # upload from agent while in time.sleep
        self.doPythonSubmit('%s 123 456 0.1 0 0' % DUMMY_PATH, ['stdout'],
                      uploadOutputs = False)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING'])
        self.app.ws.close()
        self.wait() # yeild to get on_close
        self.assertEqual(2, self.app.ws_state)
        self.app.ws_state = 3
        time.sleep(2) # wait until task finishes before reconnecting
        if self.agentMode == 'passive':
            future = websocket_connect(self.ws_req, on_message_callback = self.ws_handler.on_message)
            ioloop.IOLoop.current().add_future(future, self.ws_handler.on_connect)
        else:
            time.sleep(7)
        self.wait() # wait for agent connection
        self.assertEqual(1, self.app.ws_state)
        self.checkTaskStates(self.taskId, ['DONE'])
        self.assertEqual(self.app.ws_tasks[-1][0], str(self.taskId))
        info = self.app.ws_tasks[-1][2]
        self.assertTrue('waitTime' in info)
        self.assertTrue('runTime' in info)
        self.assertTrue('stageOutBytes' in info)
        self.assertTrue('stageOutTime' in info)
        self.assertTrue('processTime' in info)
        self.assertTrue('outputData' in info)
        self.assertEqual(1, len(info['outputData']))
        self.assertTrue('path' in info['outputData'][0])
        self.assertTrue('uri' in info['outputData'][0])

    def doStageOut(self, data):
        sendMessage(self.app.ws, 'STAGEOUT', str(self.taskId), data)
        self.wait()
        self.assertEqual('STAGEOUT_RESP', self.app.ws_other_msgs[-1][0])
        self.assertEqual(str(self.taskId), self.app.ws_other_msgs[-1][1])
        return self.app.ws_other_msgs[-1]

    def doStageIn(self, data, status='DONE'):
        sendMessage(self.app.ws, 'STAGEIN', str(self.taskId), data)
        self.wait()
        self.assertEqual('STAGEIN_RESP', self.app.ws_other_msgs[-1][0])
        self.assertEqual(str(self.taskId), self.app.ws_other_msgs[-1][1])
        self.assertEqual(status, self.app.ws_other_msgs[-1][2])

    def testLoopingTask(self):
        self.doPythonSubmit('%s 0.01 in.txt out.txt' % DUMMY_LOOP_PATH, [])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING'])

        # first successful call
        with open(os.path.join(self.file_root, 'nums.txt'), 'wb') as f:
            f.write('1 2 3 4\n'.encode())
        self.doStageIn([{
            'path' : 'in.txt',
            'uri' : self.get_url('/compute/files/nums.txt')
        }])

        time.sleep(1)

        msg = self.doStageOut([{'paths' : ['out.txt']}])
        self.assertEqual('DONE', msg[2])
        self.assertEqual(10, int(fetchRelativeURI(msg[3][0]['uri']).body))

        # the following errors are handles different ways in agent, so test both
        msg = self.doStageOut([{'paths' : ['some-nonexistent-file']}])
        # (failing tasks with missing output files is currently disabled as too restrictive)
        # self.assertEqual('FAILED', msg[2])
        # self.assertTrue('expected' in msg[4])
        self.assertEqual('DONE', msg[2])
        self.assertEqual(0, len(msg[3]))

        # second possible failure
        msg = self.doStageOut([{'paths' : ['out.txt'], 'pack' : 'out.tar.gz',
                                'uri' : self.get_url('/nothing')}])
        self.assertEqual('FAILED', msg[2])
        self.assertTrue('upload' in msg[4])

        # another successful call
        with open(os.path.join(self.file_root, 'nums.txt'), 'wb') as f:
            f.write('1 2 3 4 5\n'.encode())
        self.doStageIn([{
            'path' : 'in.txt',
            'uri' : self.get_url('/compute/files/nums.txt')
        }])

        time.sleep(1)

        msg = self.doStageOut([{'paths' : ['out.txt']}])
        self.assertEqual('DONE', msg[2])
        self.assertEqual(15, int(fetchRelativeURI(msg[3][0]['uri']).body))

        # check that agent also unpacks downloaded data
        with open(os.path.join(self.file_root, 'nums.txt'), 'wb') as f:
            f.write('1 2 3 4 5 6\n'.encode())
        inPath = os.path.join(self.file_root, 'nums.tar.gz')
        with closing(tarfile.open(inPath, 'w:gz')) as ar:
            ar.add(os.path.join(self.file_root, 'nums.txt'), 'in.txt')
        self.doStageIn([{
            'path' : 'nums.tar.gz',
            'unpack' : '.',
            'uri' : self.get_url('/compute/files/nums.tar.gz')
        }])

        time.sleep(1)

        msg = self.doStageOut([{'paths' : ['out.txt']}])
        self.assertEqual('DONE', msg[2])
        self.assertEqual(21, int(fetchRelativeURI(msg[3][0]['uri']).body))

        
        sendMessage(self.app.ws, 'TASK_CANCEL', str(self.taskId))
        self.checkTaskStates(self.taskId, ['CANCELED'])

    def testCache(self):
        self.doPythonSubmit('%s 0.01 in.txt out.txt' % DUMMY_LOOP_PATH, [])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING'])

        data = '1 2 3 4\n'.encode()
        sha1 = hashlib.sha1(data).hexdigest()
        with open(os.path.join(self.file_root, 'nums.txt'), 'wb') as f:
            f.write(data)
        start = time.time()
        for i in range(5):
            self.doStageIn([{
                'path' : 'in%d.txt' % i,
                'uri' : self.get_url('/slow/nums.txt'),
                'sha1' : sha1
            }])
        end = time.time()
        self.assertTrue(end - start < 3)

        # before testing hash missmatch, check other arguments
        self.doStageIn([{
            'path' : 'in98.txt',
            'uri' : self.get_url('/compute/files/nums.txt'),
            'sha1' : sha1
        }])
        
        # hash missmatch
        self.doStageIn([{
            'path' : 'in99.txt',
            'uri' : self.get_url('/compute/files/nums.txt'),
            'sha1' : '1234567890'
        }], 'FAILED')

        sendMessage(self.app.ws, 'TASK_CANCEL', str(self.taskId))
        self.checkTaskStates(self.taskId, ['CANCELED'])

    def testCacheConcurrentStageIn(self):
        data = '1 2 3 4\n'.encode()
        sha1 = hashlib.sha1(data).hexdigest()
        with open(os.path.join(self.file_root, 'nums.txt'), 'wb') as f:
            f.write(data)
        inputDatum = {
            'path' : 'in0.txt',
            'uri' : self.get_url('/slow/nums.txt'),
            'sha1' : sha1
        }
        self.doPythonSubmit(
            '%s -1 0 0.1 0 0 in0.txt' % DUMMY_PATH,
            inputData=[inputDatum])
        self.doPythonSubmit(
            '%s -1 0 0.1 0 0 in0.txt' % DUMMY_PATH,
            inputData=[inputDatum])
        self.checkTaskStatesSoft(self.taskId-1, ['ACCEPTED', 'STAGED_IN', 'RUNNING',
                                                 'COMPLETED', 'STAGED_OUT', 'DONE'])
        self.checkTaskStatesSoft(self.taskId, ['ACCEPTED', 'STAGED_IN', 'RUNNING',
                                               'COMPLETED', 'STAGED_OUT', 'DONE'])
        lines = []
        with open('log/agent.txt', 'r') as f:
            for l in f.readlines():
                lines.append(l)
                if len(lines) > 100:
                    lines = lines[1:]
        self.assertTrue(sha1 + ' to be downloaded by' in '\n'.join(lines))

    def testAuth(self):
        inp = self.makeInput('input')
        inputDatum = {
            'uri' : self.get_url("/auth/%s" % inp),
            'path' : inp,
            'unpack' : 'data',
            'auth' : 'CoolMethod 12345'
        }
        outputDatum = {
            'uri' : self.get_url("/auth/output.tar.gz"),
            'paths' : ['stdout'],
            'pack' : 'output.tar.gz',
            'auth' : 'CoolMethod 12345'
        }
        noAuthI = dict(inputDatum)
        del noAuthI['auth']
        noAuthO = dict(outputDatum)
        del noAuthO['auth']

        # both input and output with correct auth
        self.doPythonSubmit(
            '%s -1 0 0.1 0 0 data/input.txt' % DUMMY_PATH,
            inputData=[inputDatum], outputData=[outputDatum])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'STAGED_IN', 'RUNNING',
                                           'COMPLETED', 'STAGED_OUT', 'DONE'])
        outPath = os.path.join(self.file_root, 'output.tar.gz')
        with closing(tarfile.open(outPath, 'r:gz')) as ar:
            with open('runtests.sh', 'rb') as expf:
                resf = ar.extractfile('stdout')
                # Workaround for dummy.py printing to stdout in text mode
                self.assertEqual([l.strip() for l in expf.readlines()],
                                 [l.strip() for l in resf.readlines()])

        # input with incorrect auth
        self.doPythonSubmit(
            '%s -1 0 0.1 0 0 data/input.txt' % DUMMY_PATH,
            inputData=[noAuthI], outputData=[outputDatum])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'FAILED'])
        self.assertEqual(6, self.app.ws_tasks[-1][2]['errorCode'])

        # output with incorrect auth
        self.doPythonSubmit(
            '%s -1 0 0.1 0 0 data/input.txt' % DUMMY_PATH,
            inputData=[inputDatum], outputData=[noAuthO])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'STAGED_IN', 'RUNNING',
                                           'COMPLETED', 'FAILED'])
        self.assertEqual(6, self.app.ws_tasks[-1][2]['errorCode'])

        # input with delete attribute
        inpPath = os.path.join(self.file_root, inp)
        self.assertTrue(os.path.isfile(inpPath))
        inputDatum['delete'] = True
        self.doPythonSubmit(
            '%s -1 0 0.1 0 0 data/input.txt' % DUMMY_PATH,
            inputData=[inputDatum], outputData=[outputDatum])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'STAGED_IN', 'RUNNING',
                                           'COMPLETED', 'STAGED_OUT', 'DONE'])
        sendMessage(self.app.ws, 'TASK_DELETE', str(self.taskId))
        self.checkTaskStates(self.taskId, ['DELETED'])
        self.assertFalse(os.path.isfile(inpPath))

    def testLS(self):
        expectedLS = {
            six.u("tree/1.txt") : {
                six.u("sha1") : six.u("e5fa44f2b31c1fb553b6021e7360d07d5d91ff5e")
            },
            six.u("tree/2.txt") : {
                six.u("sha1") : six.u("7448d8798a4380162d4b56f9b452e2f6f9e24e7a")
            },
            six.u("tree/3.txt") : {
                six.u("sha1") : six.u("a3db5c13ff90a36963278c6a39e4ee3c22e2a436")
            },
            six.u("tree/sub1/4.txt") : {
                six.u("sha1") : six.u("9c6b057a2b9d96a4067a749ee3b3b0158d390cf1")
            },
            six.u("tree/sub1/5.txt") : {
                six.u("sha1") : six.u("5d9474c0309b7ca09a182d888f73b37a8fe1362c")
            },
            six.u("tree/sub1/sub3/8.txt") : {
                six.u("sha1") : six.u("136571b41aa14adc10c5f3c987d43c02c8f5d498")
            },
            six.u("tree/sub1/sub3/9.txt") : {
                six.u("sha1") : six.u("b6abd567fa79cbe0196d093a067271361dc6ca8b")
            },
            six.u("tree/sub2/6.txt") : {
                six.u("sha1") : six.u("ccf271b7830882da1791852baeca1737fcbe4b90")
            },
            six.u("tree/sub2/7.txt") : {
                six.u("sha1") : six.u("d3964f9dad9f60363c81b688324d95b4ec7c8038")
            },
            six.u("tree.tar.gz") : {
                six.u("sha1") : six.u("7b1ad243795bb8d4a32a6403bf6ac1e77783e695")
            }
        }
        shutil.copy('tree.tar.gz', os.path.join(self.file_root, 'tree.tar.gz'))

        self.doPythonSubmit('%s 0 0 0.1 0 0' % DUMMY_PATH, inputs=['tree.tar.gz'])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'STAGED_IN', 'RUNNING',
                                           'COMPLETED', 'STAGED_OUT', 'DONE'])

        sendMessage(self.app.ws, 'LISTDIR', str(self.taskId))
        self.wait()
        self.assertEqual('LISTDIR_RESP', self.app.ws_other_msgs[-1][0])
        self.assertEqual(str(self.taskId), self.app.ws_other_msgs[-1][1])
        files = self.app.ws_other_msgs[-1][2]
        del files['stdout'], files['stderr'], files['output.tar.gz']
        self.assertEqual(set(files.keys()), set(expectedLS.keys()))
        for k in files:
            self.assertEqual(files[k], expectedLS[k])

    def checkProxy(self, proxy):
        # submit task with proxy enabled
        self.taskId = self.taskId + 1
        sendMessage(self.app.ws, 'TASK_SUBMIT', str(self.taskId), {
            "command" : "{} {}".format(sys.executable, DUMMY_WEB_PATH),
            "inputData" : [],
            "outputData" : [],
            "taskEndpoint" : {"enable" : True, "proxy" : proxy}
        }, {})

        # wait for RUNNING with endpoint
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING'])
        info = self.app.ws_tasks[-1][2]
        endpoint = info['taskEndpoint']

        # do an HTTP request to the task
        with closing(httpclient.HTTPClient()) as client:
            response = client.fetch('http://%s/fast/test.html' % endpoint)
        self.assertIn('html', response.body)

        # terminate task
        sendMessage(self.app.ws, 'TASK_CANCEL', str(self.taskId))
        self.checkTaskStates(self.taskId, ['CANCELED'])

    # def testProxy(self):
    #     self.checkProxy(False)
    #     self.checkProxy(False)
    #     self.checkProxy(False)
    #     self.checkProxy(False)
    #     self.checkProxy(False)
    #     self.checkProxy(False)
    #     self.checkProxy(False)
    #     self.checkProxy(False)
    #     try:
    #         # start proxy frontend
    #         frontend = subprocess.Popen(['python', '-m', 'everest_proxy.server',
    #                                      '--backend_tokens=1234'])
    #         time.sleep(0.5)
    #         self.checkProxy(True)
    #         self.checkProxy(False)
    #     finally:
    #         frontend.terminate()
    #
    # def testProxyFailureNoFrontend(self):
    #     # submit task with proxy enabled
    #     self.taskId = self.taskId + 1
    #     sendMessage(self.app.ws, 'TASK_SUBMIT', str(self.taskId), {
    #         "command" : 'python %s' % DUMMY_WEB_PATH,
    #         "inputData" : [],
    #         "outputData" : [],
    #         "taskEndpoint" : {"enable" : True, "proxy" : True}
    #     }, {})
    #     self.checkTaskStates(self.taskId, ['ACCEPTED', 'FAILED'])
    #     self.assertEqual(6, self.app.ws_tasks[-1][2]['errorCode'])

    def testCyrillicTar(self):
        inputs = [self.makeInput(six.u('тест'))]
        self.doPythonSubmit(six.u('%s -1 0 0.1 0 0 тест.txt') % DUMMY_PATH,
                      inputs=inputs, outputFiles=[six.u('тест.txt')])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'STAGED_IN', 'RUNNING',
                                           'COMPLETED', 'STAGED_OUT', 'DONE'])

    def testCyrillicZip(self):
        inPath = os.path.join(self.file_root, six.u('тест.zip'))
        with closing(zipfile.ZipFile(inPath, 'w')) as ar:
            ar.write('runtests.sh', six.u('тест.txt'))
        self.doPythonSubmit(six.u('%s -1 0 0.1 0 0 тест.txt') % DUMMY_PATH,
                      inputs=[six.u('тест.zip')], outputFiles=[six.u('тест.txt')])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'STAGED_IN', 'RUNNING',
                                           'COMPLETED', 'STAGED_OUT', 'DONE'])

class TaskDirCleanupTest(AgentTestServer):
    def setUp(self):
        if os.path.isdir('tasks'):
            shutil.rmtree('tasks')
        moreParams = {}
        moreParams['keepTaskDir'] = True
        moreParams['taskDirCleanupPeriod'] = 2
        moreParams['taskDirSize'] = 200
        self.agentMode = 'passive'
        AgentTestServer.setUp(self, moreParams)

    def tearDown(self):
        AgentTestServer.tearDown(self)
        shutil.rmtree('tasks')

    def getTaskStderr(self, taskId):
        return os.path.join('tasks', str(taskId), 'stderr')

    def testTwoTasks(self):
        for i in range(2):
            self.doPythonSubmit('%s -1 -1 0 0 0' % DUMMY_PATH)
            self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING', 'COMPLETED',
                                               'STAGED_OUT', 'DONE'])
            sendMessage(self.app.ws, 'TASK_DELETE', str(self.taskId))
            self.checkTaskStates(str(self.taskId), ['DELETED'])
            self.assertTrue(os.path.isfile(self.getTaskStderr(self.taskId)),
                            'task %d' % self.taskId)
        time.sleep(2)
        self.assertFalse(os.path.isfile(self.getTaskStderr(self.taskId-1)))
        self.assertTrue(os.path.isfile(self.getTaskStderr(self.taskId)))

    def testSkipRunning(self):
        self.doPythonSubmit('%s -1 -1 0 0 0' % DUMMY_PATH)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING', 'COMPLETED',
                                           'STAGED_OUT', 'DONE'])
        sendMessage(self.app.ws, 'TASK_DELETE', str(self.taskId))
        self.checkTaskStates(str(self.taskId), ['DELETED'])

        self.assertTrue(os.path.isfile(self.getTaskStderr(self.taskId)))

        self.doPythonSubmit('%s -1 -1 10 0 0' % DUMMY_PATH)
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'RUNNING'])
        shutil.copy('tree.tar.gz', os.path.join('tasks', str(self.taskId)))

        time.sleep(2)

        self.assertFalse(os.path.isfile(self.getTaskStderr(self.taskId-1)))
        self.assertTrue(os.path.isfile(self.getTaskStderr(self.taskId)))

        sendMessage(self.app.ws, 'TASK_CANCEL', str(self.taskId))
        self.checkTaskStates(self.taskId, ['CANCELED'])

class TornadoTimeoutTest(AgentTestServer):
    def setUp(self):
        moreParams = {}
        moreParams['connectTimeout'] = 1
        moreParams['requestTimeout'] = 1
        self.agentMode = 'active'
        AgentTestServer.setUp(self, moreParams)

    def tearDown(self):
        AgentTestServer.tearDown(self)
        shutil.rmtree('tasks')

    def testTimeout(self):
        inp = self.makeInput('input')
        inputDatum = {
            'uri' : self.get_url("/slow/%s" % inp),
            'path' : inp,
            'unpack' : 'data'
        }
        outputDatum = {
            'uri' : self.get_url("/slow/output.tar.gz"),
            'paths' : ['stdout'],
            'pack' : 'output.tar.gz'
        }

        # both input and output with correct auth
        self.doPythonSubmit(
            '%s -1 0 0.1 0 0 data/input.txt' % DUMMY_PATH,
            inputData=[inputDatum], outputData=[outputDatum])
        self.checkTaskStates(self.taskId, ['ACCEPTED', 'STAGED_IN', 'RUNNING',
                                           'COMPLETED', 'STAGED_OUT', 'DONE'])

class PassiveAgentTest(AgentTests):
    def setUp(self):
        self.agentMode = 'passive'
        AgentTests.setUp(self)

class ActiveAgentTest(AgentTests):
    def setUp(self):
        self.agentMode = 'active'
        AgentTests.setUp(self)
