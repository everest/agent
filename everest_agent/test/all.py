# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from __future__ import absolute_import
import unittest
import sys

import tornado

# Need these imports in order to detect import errors
import everest_agent.test.cache_test
import everest_agent.test.agent_test

def all():
    tests = [
        'everest_agent.test.agent_test.TornadoTimeoutTest',
        'everest_agent.test.agent_test.TaskDirCleanupTest',
        'everest_agent.test.cache_test.CacheTest',
        'everest_agent.test.agent_test.ActiveAgentTest',
        'everest_agent.test.agent_test.PassiveAgentTest'
    ]
    return unittest.defaultTestLoader.loadTestsFromNames(tests)

if __name__ == "__main__":
    if sys.version_info[0] == 2 and sys.version_info[1] < 7:
        tornado.testing.main()
    else:
        tornado.testing.main(failfast = True)

