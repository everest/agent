#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from __future__ import absolute_import
import os
import subprocess
import re
import json
from everest_agent.exceptions import TaskException
import six
from six.moves import range

def create(config):
    return TorqueTaskHandler(config)

class TorqueTaskHandler:
    def __init__(self,config):
        if 'queue' in config:
            self.queue = config['queue']
        else:
            self.queue = None
        self.reason = ["",
        "Job execution failed, before files, no retry",
        "Job execution failed, after files, no retry",
        "Job execution failed, do retry",
        "Job aborted on MOM initialization",
        "Job aborted on MOM init, chkpt, no migrate",
        "Job aborted on MOM init, chkpt, ok migrate",
        "Job restart failed",
        "Exec() of user command failed",
        "Could not create/open stdout stderr files",
        "Job exceeded a memory limit",
        "Job exceeded a walltime limit",
        "Job exceeded a CPU time limit"]

    # TODO: support resource requirements
    def submit(self, command, dir, env, requirements):
        if not os.access(dir, os.F_OK):  
            os.makedirs(dir) 
        job_file_str = os.path.join(os.path.abspath(dir),"jobfile")
        job_file = open (job_file_str,"w")
        
        job_file.write("#PBS -d ")
        job_file.write(os.path.abspath(dir))
        job_file.write("\n#PBS -e stderr\n")
        job_file.write("#PBS -o stdout\n")
        if self.queue:
            s = "#PBS -q "+self.queue + "\n"
            job_file.write(s)
        #~ job_file.write("#PBS -k eo\n")
        job_file.write("#PBS -v EVEREST_AGENT_PORT,EVEREST_AGENT_TASK_ID,EVEREST_AGENT_TASK_USER,EVEREST_JOB_ID,EVEREST_TASK_TOKEN\n")
        job_file.write(command)
        job_file.close()

        # override and uset environment variables
        task_env = os.environ.copy()
        for k, v in six.iteritems(env):
            if v is None:
                del task_env[k]
            else:
                task_env[k] = v

        try:
            p = subprocess.Popen(["qsub",job_file_str],stdout=subprocess.PIPE,stderr=subprocess.PIPE, env=task_env)
            (id,er) = p.communicate()
            return int(id.decode().split('.',1)[0])
        except Exception as e:
            raise TaskException("Unable to initiate PBS")

    def get_state(self,pid):
        try:
            p = subprocess.Popen(["qstat", "-f",str(pid)],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (resp,er) = p.communicate()
        except Exception as e:
            raise e
        stat_m = re.search("job_state = (?P<status>\w)", resp.decode())
        if stat_m:
            if (stat_m.group('status') == 'R') or (stat_m.group('status') == 'E'):
                return "RUNNING"
            elif (stat_m.group('status') == 'Q') or (stat_m.group('status') == 'S') or (stat_m.group('status') == 'W') or (stat_m.group('status') == 'H') or (stat_m.group('status') == 'T'):
                return "QUEUED"
            elif (stat_m.group('status') == 'C'):
                exit_m = re.search("exit_status = (?P<status>-?\d+)", resp.decode())
                if exit_m:
                    exit_code = int(exit_m.group('status'))
                    if (exit_code>127):
                        return "CANCELED"
                    elif exit_code == 0:
                        return "COMPLETED"
                    else:
                        st = "FAILED ExitCode=" + str(exit_code)
                        if exit_code < 0:
                            ec = -1*exit_code
                            if ec in range(1,13):
                                st += " ("
                                st += self.reason[ec]
                                st += ")"
                        return st
                else:
                    return "CANCELED"
        else:
            raise TaskException("Unable to parse the status of the job")
        
    def cancel(self,pid):
        try:
            p = subprocess.Popen(["qdel", str(pid)])
            p.wait()
        except:
            pass
                
    def get_resource_state(self):
        state = {}
        state['type'] = 'torque'
        return json.dumps(state)
        
    def get_slots(self):
        try:
            p = subprocess.Popen(["pbsnodes"],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (resp,er) = p.communicate()
        except Exception as e:
            return -1
        else:
            num = 0
            nodes_m = re.findall("state = (?P<state>.+)\W*np = (?P<number>\d+)", resp.decode())
            for s in nodes_m:
                if not "down" in s[0] and not "offline" in s[0]:
                    num+=int(s[1])
            return num
        
    def get_free_slots(self):
        try:
            p = subprocess.Popen(["pbsnodes"],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (resp,er) = p.communicate()
        except Exception as e:
            return -1
        else:
            num = 0
            nodes_m = re.findall("state = (?P<state>.*)\W*np = (?P<number>\d+).*\W*.*\W*(jobs =.*)?", resp.decode())
            for s in nodes_m:
                if not "down" in s[0] and not "offline" in s[0]:
                    num+=int(s[1])
                    if s[2]:
                        jobs = re.findall("/",s[2])
                        num-=len(jobs)
            return num

    def get_task_stat(self,pid):
        stat = {}
        try:
            p = subprocess.Popen(["qstat", "-f",str(pid)],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            (resp,er) = p.communicate()
        except Exception as e:
            stat.update({"cpu-time":-1})
            stat.update({"wall-time":-1})
        else:
            cpu_reg = re.search("resources_used\.cput = (?P<cpuh>\d+):(?P<cpum>\d+):(?P<cpus>\d+)", resp.decode())
            wall_reg = re.search("resources_used\.walltime = (?P<wallh>\d+):(?P<wallm>\d+):(?P<walls>\d+)", resp.decode())
            
            stat.update({"cpu-time":int(cpu_reg.group("cpuh"))*3600 + int(cpu_reg.group("cpum"))*60 + int(cpu_reg.group("cpus"))})
            stat.update({"wall-time":int(wall_reg.group("wallh"))*3600 + int(wall_reg.group("wallm"))*60 + int(wall_reg.group("walls"))})
        
        return json.dumps(stat)

    def get_type(self):
        return 'torque'
