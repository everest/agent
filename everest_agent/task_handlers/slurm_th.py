#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from __future__ import absolute_import
import logging
import os
import re
import subprocess
import traceback
import json

import everest_agent.task_handlers.slurm_commons as slurm_commons
from everest_agent.exceptions import TaskException
import six


def create(config):
    return SlurmTaskHandler(config)


class SlurmTaskHandler:
    def __init__(self, config):
        self.queue = config.get('queue', None)
        self.logger = logging.getLogger("everest_agent.slurm_th")

    def get_type(self):
        return 'slurm'

    def submit(self, command, work_dir, env, requirements):
        if not os.access(work_dir, os.F_OK):
            os.makedirs(work_dir)

        job_file_str = os.path.join(os.path.abspath(work_dir), "jobfile")
        with open(job_file_str, "w") as job_file:
            job_file.write("#!/bin/sh\n")
            job_file.write("#SBATCH -D %s\n" % os.path.abspath(work_dir))
            job_file.write("#SBATCH -e stderr\n")
            job_file.write("#SBATCH -o stdout\n")
            if self.queue:
                job_file.write("#SBATCH -p %s\n" % self.queue)
            # resource requirements
            if requirements is not None:
                nodes = requirements.get('nodes', 1)
                cores_per_node = requirements.get('coresPerNode', 1)
                memory_per_core = requirements.get('memoryPerCore')
                time_limit = requirements.get('timeLimit')
                job_file.write("#SBATCH -N %d\n" % nodes)
                job_file.write("#SBATCH --ntasks-per-node=%d\n" % cores_per_node)
                if memory_per_core is not None:
                    memory_per_node = memory_per_core * cores_per_node
                    job_file.write("#SBATCH --mem=%d\n" % memory_per_node)
                if time_limit is not None:
                    # convert seconds to "minutes:seconds"
                    time_limit_str = "%d:%d" % (time_limit/60, time_limit%60)
                    job_file.write("#SBATCH --time=%s\n" % time_limit_str)
            job_file.write(command)

        # override and unset environment variables
        task_env = os.environ.copy()
        for k, v in six.iteritems(env):
            if v is None:
                del task_env[k]
            else:
                task_env[k] = v

        try:
            os.chmod(job_file_str, 0o777)
            p = subprocess.Popen(["sbatch", job_file_str], env=task_env,
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            (out, err) = p.communicate()
            job_id = out.decode().split()
            if not job_id:
                raise TaskException("Unable to initiate SLURM, stderr: %s" % err.decode())
            return int(job_id[-1])
        except TaskException:
            raise
        except Exception:
            raise TaskException("Unable to initiate SLURM: %s" % traceback.format_exc())

    def get_state(self,pid):
        return slurm_commons.get_job_state(str(pid))

    def cancel(self,pid):
        try:
            p = subprocess.Popen(["scancel", str(pid)])
            p.wait()
        except:
            self.logger.error("Failed to cancel job: %s" % str(pid))

    def get_slots(self):
        return slurm_commons.get_slots(self.queue)

    def get_free_slots(self):
        return slurm_commons.get_free_slots(self.queue)

    def get_resource_state(self):
        state = {}
        state['type'] = 'slurm'
        return json.dumps(state)
        
    def get_task_stat(self,pid):
        stat = {}
        return json.dumps(stat)
        
    def get_type(self):
        return 'slurm'
