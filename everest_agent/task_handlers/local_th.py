#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from __future__ import absolute_import
import os
import signal
import subprocess
import shlex
import sys
import time

from everest_agent.exceptions import TaskException
import six


def create(config):
    return LocalTaskHandler(config)


class LocalTaskHandler:
    def __init__(self, config):
        self.total_slots = config.get('maxTasks')
        self.free_slots = self.total_slots

    def get_type(self):
        return 'local'

    def get_slots(self):
        return self.total_slots

    def get_free_slots(self):
        return self.free_slots

    def submit(self, command, work_dir, env, requirements):
        if sys.version_info[0] == 2:
            command = list(
                map(
                    lambda s: s.decode('utf-8'),
                    shlex.split(command.encode('utf-8'))))  # encode part is for python 2.*
        else:
            command = shlex.split(command)

        if not os.access(work_dir, os.F_OK):
            os.makedirs(work_dir)
        if not os.access(work_dir, os.W_OK):
            raise TaskException("Requested directory is not writable")

        # override and uset environment variables
        task_env = os.environ.copy()
        for k, v in six.iteritems(env):
            if v is None:
                del task_env[k]
            else:
                task_env[k] = v

        with open(os.path.join(work_dir, "stdout"), "w") as std_out:
            with open(os.path.join(work_dir, "stderr"), "w") as std_err:
                try:
                    if os.name == 'posix':
                        p = subprocess.Popen(command, stdin=subprocess.PIPE,
                                             stdout=std_out, stderr=std_err,
                                             cwd=work_dir, env=task_env, preexec_fn=os.setpgrp)
                    elif os.name == 'nt':
                        # Convert relative executable path to absolute
                        # See https://bugs.python.org/issue15533
                        if command[0].startswith('./'):
                            command[0] = os.path.join(work_dir, command[0][2:])
                        p = subprocess.Popen(command, stdin=subprocess.PIPE,
                                             stdout=std_out, stderr=std_err,
                                             cwd=work_dir, env=task_env,
                                             creationflags=subprocess.CREATE_NEW_PROCESS_GROUP)
                    else:
                        raise TaskException("Unsupported OS API")
                except OSError as e:
                    raise TaskException("Unable to execute requested program (%s)" % e)

        # resource requirements
        if requirements is not None:
            task_slots = requirements.get('coresPerNode', 1)
        else:
            task_slots = 1
        self.free_slots -= task_slots

        task_info = {'popen': p, 'pid': p.pid, 'slots': task_slots, 'completed': False, 'cancelled': False}
        return task_info

    def get_state(self, task_info):
        p = task_info['popen']
        p.poll()
        if p.returncode is None:
            return "RUNNING"

        self.killpg(p)

        if task_info['cancelled']:
            return "CANCELED"
        elif not task_info['completed']:
            task_info['completed'] = True
            self.free_slots += task_info['slots']
        if p.returncode == 0:
            return "COMPLETED"
        return "FAILED ExitCode=" + str(p.returncode)

    def cancel(self, task_info):
        task_info['cancelled'] = True
        self.killpg(task_info['popen'])
        self.free_slots += task_info['slots']

    def killpg(self, p):
        if os.name == 'posix':
            try:
                os.killpg(p.pid, signal.SIGINT)
                timeout = 5
                while p.poll() is None and timeout > 0:
                    time.sleep(1)
                    timeout -= 1
                if p.returncode is None:
                    os.killpg(p.pid, signal.SIGKILL)
            except OSError:  # nothing to kill
                pass
        elif os.name == 'nt':
            try:
                p.send_signal(signal.CTRL_BREAK_EVENT)
                timeout = 5
                while p.poll() is None and timeout > 0:
                    time.sleep(1)
                    timeout -= 1
                if p.returncode is None:
                    p.kill()
            except WindowsError:
                pass
